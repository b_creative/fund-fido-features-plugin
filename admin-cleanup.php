<?php

// cleanup admin menu
add_action( 'admin_menu', 'admin_menu_cleanup');
function admin_menu_cleanup() {
	remove_menu_page( 'edit.php' ); // Posts
	remove_menu_page( 'edit-comments.php' ); // Comments
}

// cleanup the admin bar
add_action( 'admin_bar_menu', 'remove_from_admin_bar', 999 );
function remove_from_admin_bar($wp_admin_bar) {
	$wp_admin_bar->remove_node('si_menu');
	$wp_admin_bar->remove_node('comments');
	$wp_admin_bar->remove_node('new-content');
	$wp_admin_bar->remove_node('search');
	$wp_admin_bar->remove_node('wpseo-menu');
	$wp_admin_bar->remove_node('wpfc-toolbar-parent');
	$wp_admin_bar->remove_node('wp-logo');
}

// remove comment column (not used)
add_filter( 'manage_edit-page_columns', 'disable_admin_columns' );
function disable_admin_columns( $columns ) {
	unset( $columns['comments'] );
	return $columns;
}