<?php
/**!
 * Plugin Name: Fund Fido Features Plugin
 * Author: Baseline Creative | Shane Schroll
 * Author URI: https://baselinecreative.com
 * Description: Do not deactivate or delete. Supports functionality for the Fund Fido System. 
 * Version: 1.0.2
*/

// Includes
include_once( 'admin-cleanup.php' );

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'ff_enqueue_scripts' );
function ff_enqueue_scripts() {
	$plugin_url = plugin_dir_url( __FILE__ );
	$version = filemtime( $plugin_url . '/style.css' );

	wp_enqueue_style( 'ff-css', $plugin_url . '/style.css', [], $version );
}